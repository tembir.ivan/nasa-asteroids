export type Asteroid = {
  id: number;
  close_approach_data: {
    close_approach_date_full: string;
    miss_distance: {
      kilometers: string;
      lunar: string;
    };
  }[];
  estimated_diameter: {
    kilometers: {
      estimated_diameter_max: number;
    };
    meters: {
      estimated_diameter_max: number;
    };
  };
  orbital_data: {
    orbit_determination_date: string;
    last_observation_date: string;
  };
  is_potentially_hazardous_asteroid: boolean;
  name: string;
};
