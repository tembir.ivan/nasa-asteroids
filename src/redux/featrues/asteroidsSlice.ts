import { Asteroid } from "./../../entities/types";
import { createSlice } from "@reduxjs/toolkit";

type AsterdoidsSlice = {
  asteroids: Asteroid[];
  currentAsteroids: Asteroid;
  basket: Asteroid[];
};

const initialState: AsterdoidsSlice = {
  asteroids: [],
  currentAsteroids: undefined!,
  basket: [],
};

export const asteroidsSlice = createSlice({
  name: "asteroids",
  initialState,
  reducers: {
    setData: (state, action) => {
      action.payload.asteroids &&
        state.asteroids.push(...action.payload.asteroids);
      state.currentAsteroids = action.payload.currentAsteroids;
      state.basket = action.payload.basket
        ? [...action.payload.basket]
        : state.basket;
    },
  },
});

export const { setData } = asteroidsSlice.actions;
export default asteroidsSlice.reducer;
