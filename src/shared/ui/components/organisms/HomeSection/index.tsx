"use client";
import React, { useEffect, useState } from "react";
import styles from "./index.module.css";
import { useAppDispatch, useAppSelector } from "@/shared/lib/hooks";
import { setData } from "@/redux/featrues/asteroidsSlice";
import InfiniteScroll from "react-infinite-scroll-component";
import AsteroidBox from "../../molecules/AsteroidBox";

const buttons = ["в километрах", "в лунных орбитах"];

const HomeSection = () => {
  const [unit, setUnit] = useState<0 | 1>(0);
  const [page, setPage] = useState<number>(1);
  const asteroids = useAppSelector((state) => state.asteroids.asteroids);
  const dispatch = useAppDispatch();
  let counterFetch = 0;

  const getData = () => {
    fetch(
      `https://api.nasa.gov/neo/rest/v1/neo/browse?page=${page}&size=5&api_key=${process.env.NEXT_PUBLIC_API_NASA}`
    )
      .then((response) => response.json())
      .then((data) => {
        dispatch(setData({ asteroids: data.near_earth_objects }));
        setPage((e) => ++e);
      });
  };

  useEffect(() => {
    if (counterFetch === 0) getData();
    counterFetch++;
  }, []);

  return (
    <div className={styles.wrapper}>
      <h1>Ближайшие подлёты астероидов</h1>
      <div className={styles.units}>
        {buttons.map((elem, index) => (
          <React.Fragment key={index}>
            <button
              onClick={() => setUnit(index as 0 | 1)}
              className={`${styles.unit}  ${
                index === unit ? styles.active : ""
              }`}
            >
              {elem}
            </button>
            {index === 0 && <>|</>}
          </React.Fragment>
        ))}
      </div>
      <InfiniteScroll
        next={getData}
        hasMore={true}
        dataLength={asteroids.length}
        loader={undefined}
      >
        {asteroids.map((data) => (
          <AsteroidBox key={data.id} data={data} unit={unit} />
        ))}
      </InfiniteScroll>
    </div>
  );
};

export default HomeSection;
