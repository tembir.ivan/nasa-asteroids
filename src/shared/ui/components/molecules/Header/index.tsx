import React from "react";
import style from "./index.module.css";

const Header = () => {
  return (
    <header>
      <div className={style.wrapper}>
        <span className={style.title}>ARMAGEDDON 2023</span>
        <p className={`regular`}>
          ООО “Команда им. Б. Уиллиса”. Взрываем астероиды с 1998 года.
        </p>
      </div>
    </header>
  );
};

export default Header;
