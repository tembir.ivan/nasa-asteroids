import React from "react";
import Image from "next/image";
import styles from "./index.module.css";
import { Asteroid } from "@/entities/types";
import ArrowIcon from "../../atoms/Icons/ArrowIcon";
import asteroidImage from "@/assets/Images/asteroid.png";
import { useAppDispatch, useAppSelector } from "@/shared/lib/hooks";
import { setData } from "@/redux/featrues/asteroidsSlice";
import Link from "next/link";

type Props = {
  data: Asteroid;
  unit: 0 | 1;
};

const AsteroidBox = ({ data, unit }: Props) => {
  const basket = useAppSelector((state) => state.asteroids.basket);
  const dispatch = useAppDispatch();
  const normalDate = new Date(data.orbital_data.last_observation_date)
    .toDateString()
    .split(" ")
    .slice(1);
  [normalDate[0], normalDate[1]] = [normalDate[1], normalDate[0]];

  const distance =
    unit === 0
      ? Math.floor(Number(data.close_approach_data[0].miss_distance.kilometers))
      : Math.floor(Number(data.close_approach_data[0].miss_distance.lunar));

  const size = data.estimated_diameter.kilometers.estimated_diameter_max * 10;
  const fq =
    new Date(data.orbital_data.orbit_determination_date).getFullYear() + " FQ";
  const basketInclude = basket.findIndex((value) => value.id === data.id);

  const addToBasket = (event: React.MouseEvent) => {
    event.preventDefault();
    const newBasket = [...basket];
    basketInclude === -1
      ? newBasket.push(data)
      : newBasket.splice(basketInclude, 1);
    dispatch(setData({ basket: newBasket }));
  };

  return (
    <Link href={`/${data.id}`} className={styles.wrapper}>
      <h2>{normalDate.join(" ")}</h2>
      <div className={styles.info}>
        <div className={`regular ${styles.distance}`}>
          <span>
            {distance}{" "}
            {unit === 0 ? (
              <>км</>
            ) : (
              <>
                {distance < 5 ||
                (distance > 20 &&
                  Number(distance.toString()[distance.toString.length - 1]) <
                    5) ? (
                  <>лунные орбиты</>
                ) : (
                  <>лунных орбит</>
                )}
              </>
            )}
          </span>
          <ArrowIcon width={"100%"} />
        </div>
        <Image
          src={asteroidImage}
          alt={"asteroid"}
          width={size > 15 && size < 60 ? size : 30}
          height={size > 15 && size < 60 ? size : 30}
          className={styles.asteroid}
        />
        <div className={`regular ${styles.size}`}>
          <span className={styles.year}>{fq}</span>
          <span className={styles.diameter}>
            Ø{" "}
            {Math.floor(data.estimated_diameter.meters.estimated_diameter_max)}
          </span>
        </div>
      </div>
      <div className={styles.shop}>
        <button
          className={basketInclude !== -1 ? styles.active : ""}
          onClick={addToBasket}
        >
          {basketInclude !== -1 ? "В КОРЗИНЕ" : "ЗАКАЗАТЬ"}
        </button>
        {data.is_potentially_hazardous_asteroid && <span>⚠ Опасен</span>}
      </div>
    </Link>
  );
};

export default AsteroidBox;
