"use client";
import React from "react";
import styles from "./index.module.css";
import { useAppSelector } from "@/shared/lib/hooks";

const Basket = () => {
  const basket = useAppSelector(
    (state) => state.asteroids.basket
  ).length.toString();

  if (basket !== "0")
    return (
      <div className={styles.wrapper}>
        <h3>Корзина</h3>
        <span className="regular">
          {basket}{" "}
          {basket[basket.length - 1] === "1"
            ? "астероид"
            : Number(basket[basket.length - 1]) < 5
            ? "астероида"
            : "астероидов"}
        </span>
        <button className={`regular ${styles.send}`}>Отправить</button>
      </div>
    );
};

export default Basket;
