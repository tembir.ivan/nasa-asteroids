import { Asteroid } from "@/entities/types";
import styles from "./page.module.css";
import Link from "next/link";

const getAsteroidData = async (id: string) => {
  const response = await fetch(
    `https://api.nasa.gov/neo/rest/v1/neo/${id}?api_key=${process.env.NEXT_PUBLIC_API_NASA}`
  );

  if (!response.ok) throw new Error("Problem with fetching...!");

  return await response.json();
};

const AsteroidPage = async ({ params: { id } }: { params: { id: string } }) => {
  const data: Asteroid = await getAsteroidData(id);

  return (
    <section>
      <div className={styles.content}>
        <Link href={"/"}>To Main Page</Link>
        <h2>{data.name}</h2>
        <span>
          Diameter {data.estimated_diameter.meters.estimated_diameter_max}m
        </span>
        <span>etc...</span>
      </div>
    </section>
  );
};

export default AsteroidPage;
