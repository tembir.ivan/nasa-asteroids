import Image from "next/image";
import styles from "./page.module.css";
import StoreProvider from "@/redux/storeProvider";
import HomeSection from "@/shared/ui/components/organisms/HomeSection";
import earthImage from "../assets/Images/earth.png";
import Basket from "@/shared/ui/components/molecules/Basket";

export default function Home() {
  return (
    <section>
      <div className={styles.content}>
        <Image
          priority={true}
          src={earthImage}
          width={377}
          height={456}
          alt={"Earth"}
          className={styles.earth}
        />
        <StoreProvider>
          <HomeSection />
          <Basket />
        </StoreProvider>
      </div>
    </section>
  );
}
